#+title: Research
#+date: 2023-07-03T21:13:30+01:00
#+draft: false
#+description: A summary of research publications, contributions, conference papers and reviews from Peter's time researching Mozart and studying musicology.
#+tags[]: 
#+categories[]: 

This is a brief summary of various research-related writings I had published in some form during my short run of academic activity.

I did my MMus as a research degree at the University of Glasgow where I was supervised by John Butt. The plan was always to continue on to a PhD, though funding was an obstacle and I eventually decided to abandon this idea.

My research interests included (but were not exlusive to!):

+ Mozart
+ Eighteenth-century music theory
+ Tempo and meter
+ Music and religion
+ The role of editing in contemporary music

ORCID: [[https://orcid.org/0000-0001-6603-7920][0000-0001-6603-7920]]
