---
title: {{ replace .Name "-" " " | title }}
date: {{ .Date }}
draft: true
slug: {{ .Name }}
summary: {{ replace .Name "-" " " | title }}
tags: 
categories:
---